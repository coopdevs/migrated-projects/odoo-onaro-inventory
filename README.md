# Ansible inventories
This repository stores hosts informations and related variables for this specific instance of Odoo.

## Requirements

1. Clone this repo and [odoo-provisioning](https://gitlab.com/coopdevs/odoo-provisioning) in the same directory
2. If you want to test this set up locally, install [devenv](https://github.com/coopdevs/devenv/) and do:
   ```sh
   cd odoo-onaro-inventory
   devenv # this creates the lxc container and sets its hostname
   ```
3. Go to odoo-provisioning directory, make sure you are in the indicated branch as in onaro-inventory/inventory/group_vars/all.yml (odoo_provisioning_version) and install its Ansible dependencies:
   ```sh
   ansible-galaxy install -r requirements.yml
   ```
4. Run `ansible-playbook` command pointing to the `inventory/hosts` file of this repository:
   * development local mode
   ```sh
   # tell it to keep it local with limit=dev
   # don't ask for vault pass as no secrets are required for local setups
   # execute sys_admins.yml only the first time using the user root to create the other users: --user=root
   ansible-playbook playbooks/sys_admins.yml -i ../odoo-onaro-inventory/inventory/hosts --limit=dev
   ansible-playbook playbooks/provision.yml -i ../odoo-onaro-inventory/inventory/hosts --limit=dev
   ```
   * production mode
   ```sh
   ansible-playbook playbooks/provision.yml -i ../odoo-onaro-inventory/inventory/hosts --ask-vault-pass --limit=production
   ```
